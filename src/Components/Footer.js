import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return(
            <div style={footerStyle}>
                <p>Don't underestimate the <strong>Force.</strong></p>
            </div>
        )
    }
}

const footerStyle = {
    position: 'fixed',
    left: '0',
    bottom: '0',
    width: '100%',
    backgroundColor: '#5083CF',
    color: 'white',
    textAlign: 'center'
}

export default Footer;