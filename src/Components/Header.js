import React, { Component } from 'react';

class Header extends Component {
    render() {
        return(
            <div style={headerStyle}>
                <h1>Force Measurements Converter</h1>
            </div>
        )
    }
}

const headerStyle = {
    padding: '20px',
    textAlign: 'center',
    background: '#5083CF',
    color: 'white',
    fontSize: '20px',
}

export default Header;