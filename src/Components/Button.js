import React, { Component } from 'react';

class Button extends Component {
    render() {
        return(
            <div>
                <input style={buttonStyle} type="submit" value="Sumar" />
                <input style={buttonStyle} type="submit" value="Restar" />
                <input style={buttonStyle} type="submit" value="Multiplicar" />
                <input style={buttonStyle} type="submit" value="Dividir" />
                <button name="sumar" style={buttonStyle} onClick={(e) =>this.handleClick(1)}>Add</button>
                <button style={buttonStyle}>Subtract</button>
                <button style={buttonStyle}>Multiply</button>
                <button style={buttonStyle}>Divide</button>
            </div>
        )
    }
}

const buttonStyle = {
    display: 'inline-block',
    padding: '15px 25px',
    marginTop: '45px',
    marginRight: '5px',
    marginLeft: '5px',
    fontSize: '24px',
    cursor: 'pointer',
    textAlign: 'center',
    textDecoration: 'none',
    outline: 'none',
    color: '#fff',
    backgroundColor: '#1abc9c',
    border: 'none',
    borderRadius: '15px',
    boxShadow: '0 9px #999',
    hover:{
        backgroundColor: '#3e8e41'
    },
    active: {
        backgroundColor: '#1abc9c',
        boxShadow: '0 5px #666',
        transform: 'translateY(4px)',
    }
}

export default Button;