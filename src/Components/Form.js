import React, { Component } from 'react';

const url = "https://15kj3mxngf.execute-api.us-east-2.amazonaws.com/test/convert";

class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            numberFrom: 0,
            unitFrom: "n",
            numberTo: 0,
            unitTo: "d"
        }
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleChangeUnitFrom = this.handleChangeUnitFrom.bind(this);
        this.handleChangeUnitTo = this.handleChangeUnitTo.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleChangeInput(event) {
        this.setState({ numberFrom: event.target.value });
    }

    handleChangeUnitFrom(event) {
        this.setState({ unitFrom: event.target.value });
    }

    handleChangeUnitTo(event) {
        this.setState({ unitTo: event.target.value });
    }

    handleClick(id) {
        if(id === 1){
            
        }
    }

    async handleSubmit(event) {
        event.preventDefault();

        if (this.state.unitFrom !== this.state.unitTo) {
            const response = await fetch( url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    numberFrom: this.state.numberFrom,
                    unitFrom: this.state.unitFrom,
                    unitTo: this.state.unitTo
                })
            });
            const data = await response.json();
            this.setState({ numberTo: data.numberTo });

        } else {
            alert("'Unit From' and 'Unit to' selector must have diferent values");
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div style={formStyle}>
                        <label style={labelStyle}>Number From</label>
                        <input type="number" value={this.state.numberFrom} onChange={this.handleChangeInput} placeholder="Insert a number" />
                        <label style={labelStyle}>Unit From</label>
                        <select
                            value={this.state.unitFrom} 
                            onChange={this.handleChangeUnitFrom} 
                        >
                            <option value="n">Newton</option>
                            <option value="d">Dinas</option>
                            <option value="l">Libras</option>
                        </select>
                    </div>
                    <div style={formStyle}>
                        <label style={labelStyle}>Unit To</label>
                        <select
                            value={this.state.unitTo} 
                            onChange={this.handleChangeUnitTo} 
                        >
                            <option value="n">Newton</option>
                            <option value="d">Dinas</option>
                            <option value="l">Libras</option>
                        </select>
                    </div>
                    <div>
                        <button style={buttonStyle} onClick={(e) => this.handleClick(1)}>Convert</button>
                    </div>
                </form>
                <br></br>
                <br></br>
                <label>The conversion result is: {this.state.numberTo}</label>
            </div>
        )
    }
}



const formStyle = {
    padding: '2px',
    margin: '20px',
}

const labelStyle = {
    margin: '10px',
    padding: '2px'
}

const buttonStyle = {
    display: 'inline-block',
    padding: '15px 25px',
    marginTop: '35px',
    marginRight: '5px',
    marginLeft: '5px',
    fontSize: '22px',
    cursor: 'pointer',
    textAlign: 'center',
    textDecoration: 'none',
    outline: 'none',
    color: '#fff',
    backgroundColor: '#1abc9c',
    border: 'none',
    borderRadius: '15px',
    boxShadow: '0 9px #999',
    hover:{
        backgroundColor: '#3e8e41'
    },
    active: {
        backgroundColor: '#3e8e41',
        boxShadow: '0 5px #666',
        transform: 'translateY(4px)',
    }
}

export default Form;